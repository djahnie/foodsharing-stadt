<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Plugin;
use Grav\Common\Grav;
use Symfony\Component\EventDispatcher\Event;
use Grav\Plugin\Collection;


/**
 * Class MapcitiesPlugin
 * @package Grav\Plugin
 */

class MapCitiesPlugin extends Plugin
{
    public function onPluginsInitialized(): void
    {
        // Enable the main event we are interested in
        $this->enable([
            'onTwigInitialized' => ['onTwigInitialized', 0]
        ]);

        $assets = $this->grav['assets'];

        $assets->registerCollection('map-cities-assets', [
            'plugin://mapcities/assets/mapcities.js',
            'plugin://mapcities/assets/mapcities.css',
            'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js',
            'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css',
            'https://unpkg.com/leaflet-extra-markers@1.2.1/dist/js/leaflet.extra-markers.min.js',
            'https://unpkg.com/leaflet-extra-markers@1.2.1/dist/css/leaflet.extra-markers.min.css'
        ]);
        $assets->add('map-cities-assets', 90);
    }

    public function onTwigInitialized(Event $e)
    {
        $this->grav['twig']->twig()->addFunction(
            new \Twig_SimpleFunction('mapcities', [$this, 'createmapfunction'])
        );

    }

    public function createmapfunction()
    {
        $id = 'map-cities-23';
        return '
            <div id="' . $id . '" class="map-cities"></div>
            <script>
                (function(){
                  mapcitiesCreateMap(document.getElementById("'. $id .'"), ' . json_encode($this->collectMarkers()) . ');
                })();
            </script>
         ';
    }


    public function collectMarkers()
    {
    
        $page = Grav::instance()['page'];
        $collection = $page->evaluate(['@page.children' => '/stadt' ]);
        $ordered_collection = $collection->order('desc');

        $markers = [];

        foreach ($collection as $page) {
            $header = $page->header();
            if (isset($header->foodsharing)) {
                $markers[] = [
                    'title' => $page->title(),
                    'foodsharing' => $header->foodsharing,
                    'url' => $page->url(),
                ];
            }
        }


        
        return $markers;
    }
}
