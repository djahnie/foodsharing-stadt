function mapcitiesCreateMap(el, towns) {
  console.log(el, 'hi');

  var mymap = L.map(el).setView([51.3, 9.5], 6);

  L.tileLayer(
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZm9vZHNoYXJpbmctdG93bnMiLCJhIjoiY2tvMWhxYzB6MDhyazJwbHBsZW5oeGd3eSJ9.NIAVKt96BfToFO_yWmfdIg",
    {
      maxZoom: 18,
      attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: "mapbox/streets-v11",
      tileSize: 512,
      zoomOffset: -1,
    }
  ).addTo(mymap);

  var leaveIcon = L.icon({
    iconUrl: "/user/themes/quark/css/images/leave.png",
    shadowUrl: "/user/themes/quark/css/images/marker-shadow.png",

    iconSize: [27, 51], // size of the icon
    shadowSize: [40, 50], // size of the shadow
    iconAnchor: [0, 51], // point of the icon which will correspond to marker's location
    shadowAnchor: [5, 50], // the same for the shadow
    popupAnchor: [5, -30], // point from which the popup should open relative to the iconAnchor
  });


  // 
  var approvedIcon = L.ExtraMarkers.icon({
    icon: "fa-sun-o",
    markerColor: "green-dark",
    shape: "penta",
    prefix: "fa",
  });

    // 
    var pendingIcon = L.ExtraMarkers.icon({
      icon: "fa-sun-o",
      markerColor: "green-light",
      shape: "penta",
      prefix: "fa",
    });

  // for (var town of towns) {}
  for (var i = 0; i < towns.length; i++) {
    var town = towns[i];
    var lat = town.foodsharing.coordinates.lat;
    var lng = town.foodsharing.coordinates.lng;

    // choose which marker to use
    var status = town.foodsharing.status;
  
    if (status == 'approved') {
      var marker = L.marker([lat, lng], { icon: approvedIcon }).addTo(mymap);
    } else {
      var marker = L.marker([lat, lng], { icon: pendingIcon }).addTo(mymap);
    }
    // show popup with city name when hovering
    marker.bindPopup(town.title);
    marker.on("mouseover", function (e) {
      this.openPopup();
    });
    marker.on("mouseout", function (e) {
      this.closePopup();
    });
    marker.url = town.url;
    marker.on("click", function () {
      window.location = this.url;
    });
  }
}
