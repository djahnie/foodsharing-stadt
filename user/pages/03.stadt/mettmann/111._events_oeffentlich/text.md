---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: true
image_align: left
---

## Gemeinschaftliche Events
foodsharing Mettmann ist Preisträger des Innogy Klimaschutzpreises 2019 Mettmann. Die Jury besteht aus Innogy Mitarbeiter\*innen und Mettmanner Lokalpolitiker\*innen. Die Preisverleihung fand Ende November im Rathaus statt, Bürgermeister Thomas Dinkelmann nahm am Termin teil.
