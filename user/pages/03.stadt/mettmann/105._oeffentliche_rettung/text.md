---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
image_align: right
---

## Öffentliche Lebensmittelrettung
Letztes Jahr konnten wir sowohl auf dem Blotschenmarkt (Weihnachtsmarkt) als auch 
im September auf dem Heimatfest Lebensmittel retten. Auf dem Heimatfest waren 
unsere Retter*innen auch dieses Jahr wieder unterwegs. 

