---
title: Info Stände
menu: info_staende
published: true
image_align: right
---

## Info Stände

Wir infomieren regelmäßig Menschen über das Thema der Lebensmittelverschwendung in Jena. So nahmen wir erstmalig 2014 an einer Veranstaltung teil. In den darauffolgenden Jahren stieg die Präsenz unserer Gruppe auf verschiedenen Veranstaltungen, wodurch wir eine Vielzahl an Menschen erreichen konnten. Hier einige Beispiele:
- ALOTA-Einführungstage der Universität Jena 2014-2019
- Umwelttag 2015-2019
- Markt der Möglichkeiten 2019
- Klimapavillon 2019
- Critical Mass 2019 & 2020
- Klimapavillon 2020
- Parking Day 2020

Zudem haben wir bisher zwei öffentliche Workshops beim Klimapavillon Thüringen gegeben, sowie zwei Workshops bei der Public Climate School an der Friedrich-Schiller-Universität und Ernst-Abbe-Hochschule in Jena geleitet. 
