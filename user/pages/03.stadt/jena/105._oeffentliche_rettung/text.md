---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

Selbstverständlich schrecken wir auch vor öffentlichen Rettungen nicht zurück. Bereits seit 2018 retten wir Lebensmittel auf dem Jenaer Weihnachtsmarkt, dabei sind Kooperationen entstanden, die bis heute noch existent sind. Weiterhin haben wir für kurzfristige Großabholungen eine dynamische und agile Gruppe. Diese kam zum Beispiel im März und im Juli 2019 zum Einsatz, als knapp 2,5 Tonnen Pizzaschmelz vor der Tonne gerettet wurden. 
