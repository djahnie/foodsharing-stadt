---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

Bereits im zweiten Jahr in Folge wurden auf dem Trierer Weihnachtsmarkt Lebensmittel gerettet.
