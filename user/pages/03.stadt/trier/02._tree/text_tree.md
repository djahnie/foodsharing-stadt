---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Trier

- foodsharing Bezirk seit 2014
- Anzahl Kooperationen: 34
- Besonderes: Der erste Betrieb in Trier, der mit Foodsharing kooperierte und bis heute kooperiert, war Nahkauf Surges. Seit 16.5.2014 wurden 1443 Abholungen in dem Supermarkt durchgeführt und fast 120000 kg Lebensmittel gerettet. Bis heute retten 32 foodsharing-Mitglieder an sechs Tagen in der Woche verwertbare Lebensmittel vor der Tonne. Das finden wir großartig. Und auch großartig ist, dass wir mehr als 4,2 Tonnen CO2  in dieser Zeit eingespart haben.


(Stand: Dezember 2019)
