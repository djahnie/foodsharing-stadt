---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe


- Foodsaver\*innen: 251
- AbholerInnen mind. einmal im Monat: 35
- Kontakt: Schreibt uns eine Email an <trier@foodsharing.network>
