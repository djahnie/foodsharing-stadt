---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Einen Fairteiler haben wir aktuell nur der in der [Stralsunder Straße](https://foodsharing.de/?page=bezirk&bid=219&sub=fairteiler), nach dem Lockdown hoffentlich auch wieder im PWH!

Abholungen von den Tafeln werden derzeit „mobil“ geteilt. 
