---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler

Aktuell haben wir sieben Fairteiler und das foodsharing Café Raupe Immersatt mit integriertem Fairteiler.   
[Zu den Fairteilern](https://foodsharing.de/?page=bezirk&bid=48&sub=fairteiler)   
[Zum Raupe Immersatt Fairteiler](https://www.raupeimmersatt.de/)  

