---
title: Runder Tisch
menu: runder_tisch
image_align: left
---

## Runder Tisch
Ein Ernährungsrat befindet sich aktuell in Gründung. In Kürze soll wieder zu einem runden Tisch zum Thema Lebensmittelverschwendung eingeladen werden (Ministerium für Ländlichen Raum, Handelsverband), an welchem wir teilnehmen dürfen. 