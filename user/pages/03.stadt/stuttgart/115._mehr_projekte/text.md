---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
---

## Weitere lokale Initiativen
Es gibt verschiedene Kochgruppen z.B. Commons Kitchen Stuttgart. Ein besonderes Projekt in Stuttgart ist das erste foodsharing Café [Raupe Immersatt](https://www.raupeimmersatt.de/).

