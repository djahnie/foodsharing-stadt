---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Wir organisieren regelmäßig Infostände in Weinheim:

- Kochgruppe des Stadtjugendrings
- ‚Krempelmarkt‘ (Veranstaltung der Naturfreunde, dem Tauschring, dem BUND und den Grünen)
- ,Soziale Meile’

