---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
published: true
image_align: left
---

## Gemeinschaftlicher Fairteiler

Der Fairteiler steht auf städtischem Gelände an der Weinheimer Stadthalle. Er wird täglich von aktiven Foodsaver\*innen kontrolliert, gereinigt und befüllt.

"Der Schuppen verfügt über ein Ziegeldach, eine tiersichere Vorrichtung, Belüftung und – kleiner Augenzwinker – über einen eingebrannten „Anno Domini 2019“-Schriftzug. „Normalerweise machen wir kreative Sachen, das ist jetzt etwas Sinnvolles“, sagte Helmut Schmitt vom „Männerschuppen“ scherzend. Sogar die Wand hinter dem Schuppen wurde von den Arbeitern der Lern-Praxis-Werkstatt gestrichen." [Link zum Zeitungsartikel](https://www.wnoz.de/Weinheim-hat-jetzt-ein-Foodsharing-Regal-e32d8238-0c51-4867-850e-b18b5368dcf7-ds)

Die Stadt Weinheim beteiligte sich an den Kosten für den Bau des ersten Fairteilerschrankes.
