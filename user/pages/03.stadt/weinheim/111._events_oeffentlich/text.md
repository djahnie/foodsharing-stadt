---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: true
image_align: left
---

## Gemeinschaftliche Events

Die Stadt Weinheim beteiligte sich an den Kosten für den Bau des ersten Fairteilerschrankes.
Während des Lockdown 2020 übernahm foodsharing die Lebensmittelrettung bei allen Kooperationspartnern der Tafel Weinheim da diese coronabedingt schließen musste. Die Stadt unterstützte durch Öffentlichkeitsarbeit und organisatorische Aufgaben.

