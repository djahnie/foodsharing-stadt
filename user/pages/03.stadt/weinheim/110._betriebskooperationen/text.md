---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
published: true
image_align: right
---

## Betriebskooperationen

Das Restaurant ‚das Esszimmer‘ hat während dem Lockdown 2020 einen kostenlosen mobilen Mittagstisch eingerichtet.
Der Mehrgenerationentreffpunkt ‚Café Wohnzimmer‘ hat seine Räumlichkeiten während dem Lockdown 2020 für 6 Wochen als ‚Tafelersatz‘ (ausgeführt vom foodsharing Bezirk) zur Verfügung gestellt. 



