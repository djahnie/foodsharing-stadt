---
title: Info Stände
menu: info_staende
image_align: left
---

## Info Stände

Straßenfeste:

- Südwind Straßenfest,
- Veganmania

Messen:

- Argus Bike Festival,
- Autarkia Green World Tour,

Infostände mit anderen Organisationen im öffentlichen Raum:

- Jugend eine Welt
- Gemeinschaftsgarten Donaukanal
- Ohrenschmaus
- Verein M.U.T.
- Pfarre St. Elisabeth

weitere:

- LCOY (Jugendklimakonferenz)
- foodsharing Wien-Aktionstag (einen Tag sechs Infostände an stark frequentierten Standorten in der Stadt)
- 180 Degree Consulting