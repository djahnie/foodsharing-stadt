---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: left
---

## Öffentlichkeitsarbeit der Stadt

In Kooperation der Gemeinde mit dem interkommunalen Abfallentsorgungsunternehmen INTRADEL werden jährliche Sensibilisierungsaktionen oder Workshops für die Bevölkerung durchgeführt, die immer wieder auch das Thema der Vermeidung von Lebensmittelverschwendung aufgreifen.
Außerdem hat der Stadtrat im April 2019 den Beschluss gefasst, einen Aktionsplan „Plastikfreie Gemeinde“ auszuarbeiten. In diesem Zusammenhang wird in Kooperation mit dem Rat für Stadtmarketing nun ein Konzept und eine Charta erarbeitet, um zukünftig die Veranstaltungen auf dem Stadtgebiet möglichst umweltschonend und abfallarm auszurichten (Vermeidung von Einwegplastik, Mülltrennung, Förderung von sanfter Mobilität, nachhaltige Verpflegungskonzepte). Hierzu wird es am 23. November einen Workshop geben in Kooperation mit dem Organisationsteam Triathlon Eupen, zu dem alle Veranstalter eingeladen werden, um gemeinsam am Konzept zu arbeiten. Das Thema Vermeidung von Lebensmittelabfällen im Zusammenhang mit Events und die Aktivitäten von Foodsharing-Ostbelgien werden hier auch Thema sein.
Mehr Informationen gibt es auf der [Webseite der Stadt](https://www.eupen.be/leben-in-eupen/abfall-recycling/abfallvermeidung/lebensmittelverschwendung-2/)
