---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: right
---

## Lebensmittelanbau

In Schulen und Stadtvierteln fördert die Stadt die Kompostierung von nicht mehr verwertbaren Obst- und Gemüseresten und betreibt in Kooperation mit dem Sozialbetrieb BISA Schulkomposte und vier Viertelkomposte in städtischen Parks, in denen die Anwohner\*innen kostenlos ihre Küchenabfälle kompostieren können. Der Kompost kommt überwiegend den Schul- und Gemeinschaftsgärten zu Gute, so dass hier ein lokaler Stoffkreislauf gefördert wird.
Außerdem hat die Stadt Eupen in zwei städtischen Parks Flächen für Gemeinschaftsgartenprojekte zur Verfügung gestellt, um Anwohner\*innen ohne eigenen Garten den Anbau von Gemüse zu ermöglichen.
Seit 2011 ist die Stadt Eupen sogenannte Maya-Gemeinde und hat sich damit verpflichtet, im Stadtgebiet Lebensräume für Bienen und andere bestäubende Insekten zu schaffen. Neben der Bereitstellung städtischer Flächen für Bienenstöcke von lokalen Imker\*innen und der Anpflanzung von Wildblumenwiesen, hat die Stadt seit 2011 weit über 200 Obstbäume auf städtischen Grünflächen und in Parks gepflanzt. Das Obst steht für die Bevölkerung zur freien Verfügung. Seit diesem Jahr verleiht die Stadt Obstpflücker, um den Bürger*innen die Ernte zu erleichtern und zu vermeiden, dass ein Teil der Ernte sonst ungenutzt bliebe. In Kooperation mit einer mobilen Saftpresse aus der Region organisiert die Stadt seit 2017 Jahren im Herbst eine Saftpressaktion zur Ernteverwertung.

Ende November 2019 startet die erste Umsetzungsphase des Projektes „K-Ostpark – Essbarer Wald im Ostpark“. Hier werden verschiedenste Obst- und Nussbäume und Beerensträucher im Ostpark gepflanzt, um auf einer Fläche von rund 7.500 m² mit den Jahren einen „essbaren Wald“ entstehen zu lassen. Hinweistafeln zur Erntereife und Verarbeitungstipps zu den verschiedenen Früchten und Nusssorten komplettieren das Projekt.
Zudem pflanzt die Stadtgärtnerei in innerstädtische Beete und Blumenkübel verschiedene Küchenkräuter und teils Gemüse zur freien Nutzung für alle an. Zusammen mit weiteren Initiativen auf dem Stadtgebiet ergibt sich so schon fast ein Rundgang durch ein „essbares Eupen“, der weiter ausgebaut werden soll. 
