---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
---

## Bildungsangebot
Wir halten regelmäßige Vorträge in Schulen. Zuletzt hat ein Schüler der Grundschule Kettenis nach Informationen zu Foodsharing gefragt, um darüber ein Referat zu halten.
