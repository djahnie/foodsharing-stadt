---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Mit Hilfe des städtischen Projekts ["Tapetenwechsel"](https://www.tapetenwechsel-bochum.de/) konnten seit Dezember 2019 aufeinanderfolgend zwei Leerstände in der Bochumer Innenstadt als Fairteil- und Vernetzungsstandorte genutzt werden.

Die auf dem Weihnachtsmarkt 2019 geretteten Lebensmittel wurden in dem so genannten 'Weihnachtslokal' zu festen Öffnungszeiten an Besucher:innen verteilt. Neben den Fairteilungen der Lebensmittel gab es in dem sehr großen Ladenlokal die Möglichkeit für alle Menschen, mit aktiven Foodsaver\*Innen in Kontakt zu kommen, auf einem der Sofas zu verweilen, einen Tee zu trinken oder Spiele zu spielen. 

Im März 2020 sind wir dann in einen kleineren Leerstand, in die Brückstraße 22, gezogen und der sogenannte ["Brücki"](https://foodsharing.de/?page=fairteiler&bid=7&sub=ft&id=1643) wurde eröffnet . Ermöglicht wird dieser Standort durch den Verzicht des Vermieters auf die Miete und die Übernahme der Nebenkosten durch das Projekt 'Tapetenwechsel'.

Unser 24/7-Fairteiler an der Hattinger Straße 222 [(Link)](https://foodsharing.de/?page=fairteiler&bid=7&sub=ft&id=1655) wurde Anfang Juni 2020 eröffnet. Das vegane Fastfoodrestaurant Heidewitzka (https://heidewitzka-bochum.de/) unterstützt uns dort, indem uns Wasser zum Putzen des Fairteilers und Mülltonnen zur Verfügung gestellt werden.

Einer unserer [Fairteiler](https://foodsharing.de/?page=fairteiler&bid=7&sub=ft&id=1318) ist in Kooperation mit den Vereinen LutherLAB e. V. (https://www.lutherlab.de/) - einem Experimentierraum in Bochum Langendreer - und Langendreer hat's! e. V. (https://www.langendreer-hats.de/) sowie durch Förderung des Stadtteilmanagements WLAB (https://bo-wlab.de/) im August 2019 eröffnet worden. Der Fairteiler am LutherLAB ist rund um die Uhr für alle Menschen zugänglich und besteht aus zwei Schränken. Hier können zum Beispiel Gemüse, Obst oder verschlossene Trockenwaren weitergegeben oder mitgenommen werden.

In fußläufiger Nähe zum Fairteiler am LutherLAB befindet sich in Bochum-Langendreer das Naturfreundezentrum (https://naturfreundezentrum.de). In Kooperation mit der Naturfreundejugend NRW e.V. und der Ortsgruppe der Naturfreunde in Bochum-Langendreer wurde im September 2019 ein [Fairteiler](https://foodsharing.de/?page=fairteiler&bid=7&sub=ft&id=1397) eingerichtet.

An der Ruhr-Universität haben wir seit Juni 2017 einen [Farteiler](https://foodsharing.de/?page=fairteiler&bid=7&sub=ft&id=909) in Kooperation mit dem AStA. Auch die Universitätsverwaltung unterstützt uns durch die Bereitstellung der Infrastruktur für den Kühlschrank.

Für März 2021 planen wir die Eröffnung eines weiteren Fairteilers, inklusive Kühlschrank, im Stadtteilbüro in Hamme.