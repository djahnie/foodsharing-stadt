---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
published: true
image_align: left
---

## Öffentlichkeitsarbeit der Stadt

Im Zuge der Etablierung der Bochum-Fonds haben zwei Foodsaver\*Innen an einem Fotoshooting teilgenommen. Die Bilder wurden für die Plakate der Aktion und die Homepage verwendet, wo foodsharing als eines der Beispiele für erfolgreich umgesetzte Bürger\*Innenideen aus den letzten Jahren vorgestellt wird (https://bochum-fonds.de/#erfolgsgeschichten).
