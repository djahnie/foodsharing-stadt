---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Wir haben ein aktives Social Media Team, welches sich um eine Facebook- und eine Instagramseite kümmert, bei denen wir aktuell über 4.000 bzw. 1.000 Follower haben. Außerdem stehen wir im Kontakt zum WDR, Radio Bochum und weiteren vor allem lokalen Medien, welche bereits im Radio, Fernsehen und an anderen Stellen über uns berichtet haben. 