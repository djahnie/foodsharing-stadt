---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 61   
- Engagierte über Abholungen hinaus: ca. 10
- Kontakt: <saarburg.kell.konz@foodsharing.network> 

Wir haben am 1. April 2020 unser erstes FoodsaverInnen treffen zu dem alle FoodsaverInnen im Bezirk eingeladen sind. Ein offenes Ohr für die Verantwortlichen und die AbholerInnen steht für uns an erster Stelle. Bei den Treffen findet ein Austausch zu aktuellen Themen, dem Stand der Kooperationen, angedachten Neuerungen und kommenden Veranstaltungen statt. Außerdem besteht in lockerer Atmosphäre die Gelegenheit, sich kennenzulernen. Die Treffen werden protokolliert, sodass auch FoodsaverInnen, die nicht dabeisein können informiert sind. Leider musste unser erstes Treffen aufgrund von CoViD-19 ausfallen.
