---
title: Events
menu: events
published: true
image_align: left
---

## Events

**Küche für Alle (Küfa)**

Gemeinsam schnippeln, kochen, essen und genießen. Das ist das Motto der Küche für alle Konz.
Die Küfa trifft sich 1x im Monat. Alle Menschen sind willkommen. Einzige Bedingung ist ein respektvoller Umgang miteinander.
In Saarburg findet ein Gespräch mit dem Jugendzentrum im März statt. Auch Saarburg möchte die Küche für Alle anbieten.
