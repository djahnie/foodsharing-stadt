---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
background: 
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: 360
- AbholerInnen mind. einmal im Monat: ca. 60
- Engagierte über Abholungen hinaus: ca. 15-20
- Kontakt: <bayreuth@foodsharing.network>