---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
published: true
image_align: left
---

## Gemeinschaftlicher Fairteiler

Unser zweiter Fairteiler steht in der Stadtbibliothek und ist zu den dortigen Öffnungszeiten erreichbar.