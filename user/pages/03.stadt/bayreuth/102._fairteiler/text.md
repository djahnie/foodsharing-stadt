---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler

Wir haben zwei Fairteilerschränke.
Einer ist frei zugängich auf dem Geländer der Campusuni Bayreuth. Infos zum zweiten findest du unter der Frucht [Fairteiler mit Beteiligung der öffentlichen Hand](#gemeinschaftlicher_fairteiler).
