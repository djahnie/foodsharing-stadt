---
title: Bayreuth
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.9416
        lng: 11.57913
    status: pending

content:
    items: @self.modular
---
