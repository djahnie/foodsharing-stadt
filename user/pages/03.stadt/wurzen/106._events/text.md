---
title: Events
menu: events
image_align: left
---

## Events

Im Projekthaus ,Kanthaus in Wurzen fand im Sommer 2019 im Rahmen des [Sommerfestes](https://kanthaus.online/de/blog/2019-10-02_sommerfest) eine Schnippelparty statt, die dank einer großen Gemüseladung, die bei der Grimmaer Tafel abgeholt werden konnte, zum vollen Erfolg wurde. Es wurden so in einer extra aufgebauten Outdoorküche zwei leckere Gerichte gekocht.
