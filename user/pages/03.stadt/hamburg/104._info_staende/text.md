---
title: Info Stände
menu: info_staende
published: true
image_align: right
---

## Info Stände

Wir betreiben regelmäßig Infostände auf Messen, Straßenfesten, etc. beispielsweise auf dem Heldenmarkt oder der Altonale.