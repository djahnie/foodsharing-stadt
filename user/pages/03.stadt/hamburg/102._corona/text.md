---
title: foodsharing in Zeiten von Corona
menu: corona
published: right
image_align: right
---

## foodsharing in Zeiten von Corona

In Zeiten von Corona steht für uns ganz klar das Motte "Menschenleben retten geht vor Lebensmittel retten" im Vordergrund. In diesem Zuge stehen wir nicht nur mit anderen foodsharing Städten, sondern auch der deutschlandweiten foodsharing Hygiene AG und dem Hamburger Ordnungsamt im engen Austausch. In Absprache haben wir zahlreiche Regelungen für die Rettungen zu Coronazeiten eingeführt und via Forumsthreads, Pinnwandeinträgen in Betrieben und Events publiziert.

Weitere Infos im [Blogartikel](/blog/2020-06-03_corona) 