---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Unser Fairteiler steht in der Seniorenresidenz Goldener Grund direkt im Eingangsbereich. Er wird mehrmals wöchentlich befüllt und das Angebot sehr gut angenommen. Viele Bewohner\*Innen der Residenz sind nicht mehr sehr mobil, somit ist unser Fairteiler ein gutes Angebot für sie. Mittlerweile teilen auch die Bewohner\*Innen ihre Lebensmittel dort.