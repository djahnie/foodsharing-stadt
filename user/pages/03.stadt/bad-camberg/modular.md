---
title: Bad Camberg
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
 foodsharing:
    coordinates:
        lat: 50.29845
        lng: 8.26256
    status: pending

content:
    items: @self.modular
---
