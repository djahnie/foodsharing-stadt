---
title: Coburg
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 50.25819
        lng: 10.96458
    status: pending

content:
    items: @self.modular
---
