---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler

Wir haben 3 Fairteiler, davon ist einer ein Regal mit 2 großen 
Plastikboxen in einem privaten überdachten Hauseingang.
Ein weiterer Fairteiler ist ein Regal im Stadtbüro der Diakonie. Diese 
haben auch 2 große Boxen, die außerhalb der Öffnungszeiten bei 
Bedarf vor der Tür stehen. Unser neu eröffneter Fairteiler ist ein Kühlschrank, der im Mehrgenerationenhaus steht.
