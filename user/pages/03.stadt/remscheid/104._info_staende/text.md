---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

Wir haben schon mehrere Infostände in verschiedenen Stadtteilen und bei Initiativen organisiert:
- Stadtteilfest Lüttringhausen
- Grillcontest, Sommer 2019
- Stadtteilfest Hasten, Sommer 2019
- Jugendtag Lennep, Sommer 2019
- ideeller Weihnachtsmarkt Lennep
- Nacht der Kultur, Oktober 2019
(Kraftstation), Infostand und Fairteilung
-Denkerschmette, November 2019, Vortrag und Fairteilung
