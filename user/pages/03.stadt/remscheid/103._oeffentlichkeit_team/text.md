---
title: oeffentlichkeit_team
menu: oeffentlichkeit_team
published: true
image_align: right
---

## Öffentlichkeitsarbeit Team

Unsere Öffentlichkeitsarbeit wird koordiniert von den beiden Botschafterinnen, Elisabeth Erbe
und Mirjam Starke. Schreibt ihnen gerne eine [Email](remscheid@foodsharing.network).

Ziele:
-bessere Vernetzung mit der öffentlichen Hand
– runder Tisch mit diversen Initiativen aus dem sozialen, kirchlichen oder öffentlichen Bereich, sowie Umweltorganisationen und Politik
– Bildungsarbeit in Schulen und anderen Jugendorganisationen intensivieren 
– foodsharing zugänglicher machen
für Senior\*innen und Menschen, die nicht über facebook/Internet oder whatsapp erreichbar sind.
geplante Aktionen:
– Kochkurs mit schwer erreichbaren Jugendlichen 
– Infoveranstaltungen bei Jugendeinrichtungen, Umweltorganisationen, Kirche und andere caritative Einrichtungen
– „Lange Tafel Remscheid“ Essen für alle ...gemeinsames Kochen und Essen für alle Remscheider Bürger\*innen an einer langen Tafel. Organisiert von einem Zusammenschluss von öffentlicher Hand, Tafel, foodsharing und anderen Initiativen (zerowaste?)
