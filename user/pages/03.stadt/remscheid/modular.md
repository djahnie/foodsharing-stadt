---
title: Remscheid
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.17987
        lng: 7.19437
    status: approved

content:
    items: @self.modular
---
