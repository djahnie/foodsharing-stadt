---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
published: false
image_align: left
---

## Öffentlichkeitsarbeit der Stadt

2018 wurde foodsharing Remscheid mit
aufgenommen auf die Webseite der 
technischen Betriebe Remscheid
(Abfallwirtschaft/Abfallvermeidung).
Auch Privatpersonen rufen uns seitdem
an, wenn sie größere Mengen
Lebensmittel abgeben möchten, z.Bsp.
den Inhalt des Gefrierschranks nach einem Todesfall.
Besonders gefreut haben wir uns, als im
November 2019, 17 foodsaver
ausgezeichnet wurden mit der
Ehrenamtsnadel des Landes NRW.
Dafür sind mindestens 5 Std./Woche an
ehrenamtlicher Tätigkeit gefordert und
das seit mindestens 2 Jahren.
Bei der feierlichen Verleihung durch den
Oberbürgermeister Burkhard MastWeisz, konnten wir erreichen, dass foodsharing auch auf der Webseite der Stadt verlinkt wird. Ein toller Erfolg!
Außerdem startete jetzt auch eine offizielle Kooperation mit dem Rathaus: Wir dürfen nun die Buffetreste der Ratssitzungen sowie anderer städtischer Veranstaltungen retten. Ein wichtiger Schritt in Richtung foodsharing-Stadt!

