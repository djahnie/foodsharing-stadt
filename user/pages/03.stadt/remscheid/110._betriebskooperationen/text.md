---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
published: false
image_align: right
---

## Herausragende Betriebskooperationen
Eurofood (Großhändler für Obst & Gemüse) (2019) und real,- Markt (2018) waren sofort bereit, uns frische Erdbeeren zu spenden für unser Marmeladen-Koch-Projekt in der Schule. So haben wir den Schüler\*innen praktische Möglichkeiten der Nachhaltigkeit und Lebensmittelwertschätzung vermittelt. Ein Teil der Marmelade wurde anschließend im Kiosk der Genossenschaft der Schüler\*innen verkauft. 
Edeka Rötzel war sofort einverstanden und half aktiv mit als uns ein Filmteam vom WDR einen Tag lang begleitete, um die Arbeit von foodsharing einem breiterem Publikum zugänglich zu machen.



