---
title: Events
menu: events
image_align: left
---

## Events


Es findet jeden Mittwoch ein foodsharing Stammtisch statt im Radio Helsinki in der Schönaugasse 8 in Graz. foodsharing Infoveranstaltungen werden außerdem regelmäßig im Kollektivcafé „Gmota“ organisiert.