---
title: Weitere lokale Initiativen
menu: mehr_projekte
image_align: right
---

## Weitere lokale Initiativen

 Es gibt verschiedene Initiativen wie bspw. das „gemeinsame Abendessen“ beim Büro der Nachbarschaften, beim Stadtteilzentrum Triester Siedlung und auch im Kollektivcafé „Gmota“. Dort wird jeweils gerettetes Essen in Kooperation mit foodsharing bzw. in direkter Kooperation mit Restaurants wie dem „Mangolds“ organisiert und interessierten Personen zur Verfügung gestellt. Dadurch soll die Aufmerksamkeit auf die Lebensmittelverschwendung in Österreich gerichtet werden und die Gemeinschaft der jeweiligen Zentren gestärkt werden. Die „gemeinsamen Abendessen“ bekommen dadurch eine gemeinnützige und verbindende Aufgabe auf sozialer und nachhaltiger Ebene. Aber auch die Initiative „Robin Food“ macht regelmäßig in der Fußgängerzone „Herrengasse“ auf Lebensmittelverschwendung in Österreich aufmerksam, indem gedumpsterte Lebensmittel zur freien Verfügung vor Ort verschenkt werden.

