---
title: Lebensmittelanbau
menu: lebensmittelanbau
image_align: left
---

## Lebensmittelanbau

 In Graz gibt es mehrere Gemeinschaftsgärten und urban Garden Projekte. Diese werden teilweise von „Jugend am Werk“ organisiert, aber auch von kleineren Nachbarschaftsgruppen. Über das „Forum urbanes Gärtnern“ können sich die urbanen Gärten auf einer Plattform präsentieren und werden durch regelmäßige Vernetzungstreffen miteinander verbunden und in Kontakt gebracht.

<http://urbanes-gaertnern.at/>
 



