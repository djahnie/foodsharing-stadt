---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler

Alle unsere Fairteiler stehen auf öffentlichem Grund, sind rund um die Uhr zugänglich und werden von den Foodsaver*innen des Bezirkes auch regelmäßig sauber gemacht, aber auch die Bevölkerung beteiligt sich beim Säubern.

Unter diesem [Link](http://tiny.cc/foodsharing) finden sich alle unsere Fairteiler mit Fotos.