---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
image_align: left
---

## Herausragende Betriebskooperationen

Eine örtliche Bio-Supermarktkette „Denn’s“ kooperiert seit längerem mit Foodsharing Graz. Aber auch das vegane Restaurant „Ginko“ und das vegan/vegetarische Restaurant „Die Erde“ sind bereits seit Langem Kooperationspartner von Foodsharing Graz. Diese drei Betriebe überlassen uns ihre Lebensmittel und versuchen, durch eigene Öffentlichkeitsarbeit in Form von Veranstaltungen und Infoblättern auf nachhaltige Ernährungsformen aufmerksam zu machen.




