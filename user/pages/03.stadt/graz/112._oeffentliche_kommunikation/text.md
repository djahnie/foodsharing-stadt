---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
image_align: right
published: true
---

## Öffentlichkeitsarbeit der Stadt
Foodsharing Graz wurde 2018 mit dem Umweltpreis des Umweltamtes des Stadt Graz 2018 ausgezeichnet. Für das Engagement zum Thema Lebensmittelretten und der damit verbundenen Bewusstseinsarbeit in Bezug auf Lebensmittelverschwendung in Österreich wurden Foodsharing und ähnliche Projekte von weiteren Initiativen aus Graz ausgezeichnet. Zu diesen Initiativen gehören bspw. Stadtteilzentren bzw. Nachbarschaftszentren, Kollektivcafés und Privatpersonen. Wir danken an dieser Stelle ganz besonders [Atmosfair ](https://www.atmosfair.de/de/)und [Flixbus](https://www.flixbus.de/), die uns durch freiwillige Beiträge der Flixbuskunden ein tolles Elektrolastenrad "LaRa" gesponsert haben.
 
Durch die oben angesprochene Preisverleihung des Umweltpreises wurden Projekte und Initiativen rund ums Thema Lebensmittelretten thematisiert. Im weiteren wurde ein Leitfaden für die Bürger\*innen erstellt zur Vermeidung von Lebensmittelverschwendung. Auf der [Seite des Land Steiermark](http://www.nachhaltigkeit.steiermark.at/cms/beitrag/12367831/125052116) findet man die Fairteiler von Foodsharing Graz aufgelistet und eine Weiterleitung zur Foodsharing Homepage.
  
Das Umweltamt der Stadt Graz hat eine Karte erstellt auf der karitative Einrichtungen wie bspw. das Rote Kreuz, Marienstüberl und der Vinzimarkt sowie die Fairteiler in Graz aufgelistet sind. Dadurch wird sichtbar gemacht, wo Lebensmittel weitergegeben  werden können bzw. wie im Falle der Fairteiler, erhalten werden können.


