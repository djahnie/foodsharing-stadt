---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
published: true
image_align: left
---

## Öffentlichkeitsarbeit der Stadt
Der Umweltpreis 2018 des BUND Hof ging gemeinsam an die Hofer Tafel und an foodsharing Hof. Am Folgetag hatten drei Botschafter\*innen von foodsharing Hof einen Termin beim OB Dr. Harald Fichtner, um ihm von foodsharing zu berichten.  Im Juni 2019 besuchte Herr Dr. Fichtner unseren Infostand beim Umwelttag am Theresienstein.
