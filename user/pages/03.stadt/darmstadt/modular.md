---
title: Darmstadt
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.87278
        lng: 8.65126
    status: pending

content:
    items: @self.modular
---
