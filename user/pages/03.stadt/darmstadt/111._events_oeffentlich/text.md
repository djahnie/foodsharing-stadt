---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
image_align: left
---

## Gemeinschaftliche Events

Beworben werden die Aktionen durch unsere Webseite foodsharing-darmstadt.de oder durch lokale Zeitungsartikel. Realisiert durch Kooperationen mit kirchlichen/ städtischen Träger\*innen wie auch Privatpersonen. Das Gesundheitsamt arbeitet gut mit uns zusammen. 
