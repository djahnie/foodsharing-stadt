---
title: Fairteiler
menu: Fairteiler
image_align: left
---

## Fairteiler
In Darmstadt gibt es einen Fairteiler in der [Hochschule](https://foodsharing.de/?page=fairteiler&bid=25&sub=ft&id=545) und
einen in der [Uni](https://foodsharing.de/?page=fairteiler&bid=25&sub=ft&id=37).
