---
title: Bildungsangebot
menu: bildungsangebot
image_align: right
---

## Bildungsangebot

Wir organisieren verschiedene Workshops, auch mit Kindern
und Jugendlichen. Zum Teil in den Kirchen oder
in Schulen.
Wir halten Vorträge und Ähnliches in Universitäten oder anderen Bildungseinrichtungen nach
Absprache mit den Bildungsstätten.