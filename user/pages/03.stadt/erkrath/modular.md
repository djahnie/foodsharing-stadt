---
title: Erkrath
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 51.223
        lng: 6.91381
    status: pending

content:
    items: @self.modular
---
