---
title: Öffentlichkeitsarbeit der Stadt
menu: oeffentliche_kommunikation
published: true
image_align: left
---

## Öffentlichkeitsarbeit der Stadt
Beim VHS Erkrath Aktionstag Nachhaltigkeit gab es einen foodsharing Infostand plus Schnippeln aus geretteten Lebensmitteln. Einen Artikel dazu findet ihr [hier](https://www.wz.de/nrw/kreis-mettmann/erkrath/erkrathernachhaltigkeitsmesse-empfiehlt-verwenden-stattverschwenden_aid-39646925)
