---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Erkrath
- foodsharing Bezirk seit März 2019
- Anzahl Kooperationen: 1
- Besonderheiten: In dieser dicht besiedelten Gegend sind die Grenzen fließend, viele Foodsaver\*innen melden sich in mehreren Bezirken an und sind dort unterschiedlich aktiv. Die Bezirke helfen sich gegenseitig bei Einführungsabholungen, Infoständen und anderen Aktionen. 

(Stand: Dezember 2019)


