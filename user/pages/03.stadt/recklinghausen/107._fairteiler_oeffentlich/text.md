---
title: Gemeinschaftlicher Fairteiler
menu: gemeinschaftlicher_fairteiler
published: true
image_align: left
---

## Gemeinschaftlicher Fairteiler
Durch das Projekt „Zukunftswerkstatt“ der Agentur für Arbeit Recklinghausen konnte unser Fairteiler im Rahmen eines Qualifizierungsprojektes mit Jugendlichen gebaut und aufgestellt werden.  
