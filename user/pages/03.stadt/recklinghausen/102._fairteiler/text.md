---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler
2015 fingen wir mit einer kleinen Holzhütte als Fairteiler an. Seit diesem Jahr (2020) haben wir einen neuen Fairteiler. Wir haben uns für eine offenere Form entschieden, damit direkt sichtbar ist, wenn der Fairteiler befüllt ist. Dieser ist zu jeder Zeit öffentlich zugänglich und befindet sich im Eingangsbereich des soziokulturellen Zentrums Altstadtschmiede e.V.. Hier ist viel Publikumsverkehr und unser Fairteiler immer im Blick. Die Altstadtschmiede hat zudem einen Teil der Finanzierung des Schranks übernommen und unterstützt die Idee des Teilens. In der Nähe des Fairteilers steht zum Beispiel auch ein öffentlicher Bücherschrank.

Wir möchten die Idee des Lebensmittel - Fairteilers verbreiten. Dazu führen wir zurzeit mit Vertreter\*innen der Kirchengemeinden Gespräche, inwieweit dort weitere Fairteiler, für die die Gemeinden die Verantwortung übernehmen, aufgestellt werden können. Bis jetzt haben wir sehr positive Rückmeldungen und engagieren uns weiterhin für diese Idee. 

