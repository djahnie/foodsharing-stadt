---
title: Herausragende Betriebskooperationen
menu: betriebskooperationen
published: true
image_align: right
---

## Herausragende Betriebskooperationen
Unsere Kooperation mit der Bäckerei Bomheuer zeichnet sich auch durch unsere Zusammenarbeit mit der Tafel aus: Wir retten fünfmal in der Woche Backwaren, an den anderen beiden Tagen kommt die Tafel, so dass keine Lebensmittel des Betriebes im Müll landen.

Das Feinkostgeschäft Naturgarten kooperiert seit über vier Jahren mit foodsharing Recklinghausen. Die Inhaberin verarbeitet nicht verkauftes Obst und Gemüse weiter zu Marmeladen oder Salate und bietet diese im Laden an.

Auf dem Wochenmarkt unterstützen sich die Stände gegenseitig. Sie geben übrig gebliebene Waren untereinander weiter und es gibt eine enge Zusammenarbeit mit anderen Initiativen z.B. mit der Nachhaltigkeits- und Fairtrade-Initiative "Lokale Agenda 21", die ebenfalls einen Stand betreibt.



