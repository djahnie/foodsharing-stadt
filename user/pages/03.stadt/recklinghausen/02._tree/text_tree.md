---
title: Aktivität
menu: Baum
image_align: left
image: true
image_tree: tree_0_4.png        


          
    
---

Kurzbeschreibung von Recklinghausen

- foodsharing Bezirk seit 2015
- Anzahl Kooperationen: 9
- Art der Kooperationen: Bioläden, Bäckerei, Supermarkt, regionale Lebensmittelerzeuger über den Wochenmarkt, Hofladen, Tafel. 
- Besonderes: Wir konzentrieren uns in unseren Kooperationen auf kleinere, möglichst inhabergeführte Geschäfte, um den regionalen Bezug zu stärken. Seitdem unsere foodsharing Gruppe in diesem Jahr 2020 viele neue foodsaver\*innen gewinnen konnte, sind wir momentan dabei, die Kooperation mit einer Supermarktkette nach und nach auszubauen. 





(Stand: Dezember 2020)

