---
title: Gemeinschaftliche Events
menu: gemeinschaftliche_events
published: true
image_align: left
---

## Gemeinschaftliche Events

In Zusammenarbeit mit den Entsorgungsbetrieben der Stadt machte Foodsharing Mainz mit einer „Mülltonnenaktion“ aufmerksam auf die riesigen Mengen an Lebensmitteln, die innerhalb von 24 Stunden weggeworfen werden würden. Die Entsorgungsbetriebe stellten hierzu neue Biotonnen zur Verfügung. Diese wurden mit den geretteten Lebensmitteln gefüllt und dann an Passant\*innen verteilt. Es waren noch weitere Tische nötig um die Menge an Lebensmittel unterzubringen. 
