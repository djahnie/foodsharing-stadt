---
title: Lokale foodsharing Gruppe
menu: lokale_gruppe
published: true
image_align: right
---

## Lokale foodsharing Gruppe

- Foodsaver: etwa 300 aktive foodsaver\*innen, 5 Botschafter\*innen
- AbholerInnen mind. einmal im Monat:  200-300 
- Engagierte über Abholungen hinaus:  ca. 90  

Stand November 2020 haben wir 345655 kg Lebensmittel gerettet bei 39680 Rettungseinsätzen. 
Durch Corona/Covid19 sind auch noch viele Tafelersatzabholungen dazugekommen und es konnten sich spontan viele neue Teams bilden.
