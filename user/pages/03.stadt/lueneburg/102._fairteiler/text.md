---
title: Fairteiler
menu: Fairteiler
published: true
image_align: right
---

## Fairteiler
Momentan gibt es fünf Fairteiler in Lüneburg. Den Ritterstraßen-Fairteiler gibt es schon seit 2015 und den Bethlehem-Fairteiler seit November 2017. 2020 folgten Paul-Gerhardt-Fairteiler und Genezareth-Fairteiler und im Februar 2021 ist der Bockelsberg-Fairteiler dazu gekommen. Aktuell entsteht initiiert durch foodsharing Lüneburg in der Nachbargemeinde Adendorf ein weiterer Fairteiler.

• Ritterstraßen-Fairteiler" in der Ritterstraße 3: Hauseingang mit einem großen Regal, ohne Kühlschrank. [Zum Fairteiler](https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=730) 

• "Bethlehem-Fairteiler" in der Friedenstraße 8: Holzhütte mit Kühlschrank.
[Zum Fairteiler](https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=985) 

• "Paul-Gerhardt- Fairteiler" in der Bunsenstraße 82: Holzhäuschen mit Kühlschrank.
[Zum Fairteiler](https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=1750) 

• "Genezareth-Fairteiler", am Springintgut 6a: Großer Schrank, in der warmen Jahreszeit mit Kühlschrank. [Zum Fairteiler](https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=1815) 

• "Bockelsberg-Fairteiler" in der Wichernstraße 32: Holzhütte mit Kühlschrank.
[Zum Fairteiler](https://foodsharing.de/?page=fairteiler&sub=ft&bid=158&id=1942)  
