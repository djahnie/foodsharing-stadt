---
title: Info Stände
menu: info_staende
published: true
image_align: left
---

## Info Stände

2018 standen wir in St. Ingbert auf dem Parkfest und haben über Lebensmittelverschwendung und unsere Arbeit informiert. Es bestand großes Interesse und danach wuchs unsere Gruppe auch recht stark.
