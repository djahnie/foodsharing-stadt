---
title: Fruechte
menu: Fruechte
class: small

features:
     - header: lokale foodsharing Gruppe
       url: "#lokale_gruppe"
       icon: /user/images/icons/01_idea.jpg

     - header: foodsharing in Zeiten von Corona
       url: "#corona"
       icon: /user/images/icons/corona.jpg 

     - header: Fairteiler
       url: "#fairteiler"
       icon: /user/images/icons/02_idea.jpg

     - header: Öffentlichkeitsarbeit Team
       url: "#oeffentlichkeit_team"
       icon: /user/images/icons/03_idea.jpg

     - header: Info Stände
       url: "#info_staende"
       icon:  /user/images/icons/04_idea.jpg

     - header: Öffentliche Lebensmittelrettung
       url: "#oeffentliche_rettung"
       icon: /user/images/icons/05_idea.jpg

     - header: Events
       url: "#events"
       icon: /user/images/icons/06_idea.jpg

    # - header: Gemeinschaftlicher Fairteiler
    #   url: "#gemeinschaftlicher_fairteiler"
    #   icon: /user/images/icons/07_idea.jpg

     - header: Bildungsangebot
       url: "#bildungsangebot"
       icon:  /user/images/icons/08_idea.jpg

    # - header: Beratungsteam für Betriebe
    #   url: "#betriebe_team"
    #   icon: /user/images/icons/09_idea.jpg

     - header: Herausragende Betriebskooperationen
       url: "#betriebskooperationen"
       icon: /user/images/icons/10_idea.jpg

     - header: Gemeinschaftliche Events
       url: "#gemeinschaftliche_events"
       icon: /user/images/icons/11_idea.jpg

    # - header: Öffentlichkeitsarbeit der Stadt
    #   url: "#oeffentliche_kommunikation"
    #   icon: /user/images/icons/12_idea.jpg

    # - header: Integrierte Bildungsarbeit
    #   url: "#bildung"
    #   icon: /user/images/icons/13_idea.jpg

    # - header: Runder Tisch
    #   url: "#runder_tisch"
    #   icon: /user/images/icons/14_idea.jpg

     - header: Weitere lokale Initiativen
       url: "#mehr_projekte"
       icon: /user/images/icons/15_idea.jpg

    # - header: Lebensmittelanbau
    #   url: "#lebensmittelanbau"
    #   icon: /user/images/icons/16_idea.jpg

    # - header: Anstellung bei der Stadt
    #   url: "#mitarbeit_stadt"
    #   icon: /user/images/icons/17_idea.jpg

     - header: Förderung von Alternativen
       url: "#alternativen"
       icon: /user/images/icons/18_idea.jpg

           
    
---

