---
title: St. Ingbert
menu: Home
onpage_menu: false
body_classes: 

# information about foodsharing town: coordinations and status (pending/ approved)
foodsharing:
    coordinates:
        lat: 49.27880
        lng: 7.11568
    status: pending

content:
    items: @self.modular
---
