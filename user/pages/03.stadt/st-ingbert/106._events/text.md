---
title: Events
menu: events
published: true
image_align: left
---

## Events

Es fanden bereits zwei öffentliche Infoabende in der Biosphären-VHS statt, bei denen über Lebensmittelverschwendung und die Arbeit von foodsharing referiert wurde und Fragen der Teilnehmer beantwortet wurden.
