---
title: Öffentliche Lebensmittelrettung
menu: oeffentliche_rettung
published: true
image_align: right
---

## Öffentliche Lebensmittelrettung

Bei unserer Aktion auf dem Parkfest wurden uns spontan von anderen Teilnehmer\*Innen übriggebliebene Lebensmittel abgegeben, die dann auch weiter fairteilt wurden.
