---
title: Events
menu: events
published: true
image_align: right
---

## Events

Der Willkommenskreis Diez bietet Kochkurse an, bei denen wir in Zukunft einen foodsharing-Stand haben werden. Hier sind, sobald es die Krise zulässt wieder regelmäßig Schnippel-Veranstaltungen geplant, bei denen wir gerettete Lebensmittel in ein wunderbares Büffet verwandeln. 
Wir haben im Rahmen der Interkulturellen Woche in Diez am Abschlussfest auf dem Marktplatz Lebensmittel verteilt und auch beim Faschingsumzug haben die „Diezer bunten Früchtchen“ für Aufmerksamkeit gesorgt und es gab gerettetes Obst für alle. 



