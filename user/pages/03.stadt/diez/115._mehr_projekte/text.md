---
title: Weitere lokale Initiativen
menu: mehr_projekte
published: true
image_align: right
---

## Weitere lokale Initiativen

•	Es gibt seit langem einen Bioladen, der keine Lebensmittel verschwendet.

•	Außerdem wird in kürzester Zeit ein Unverpacktladen öffnen, der auch viele Workshops und Informationen zum Thema rund um die Ressource Lebensmittel anbieten wird. 

•	Aktionsgemeinschaft Blühende Lebensrãume, die sich um mehr Blüten und Artenvielfalt kümmern.

•	In den Diezer Geschäften liegen bereits mehrere Hundert „boomerang bags“ gratis aus. Diese selbstgenähten Taschen, werden in Diez verteilt und haben das Ziel Plastiktüten aus Diez zu verbannen. 

•	Fridays for Future sind regelmäßig aktiv

•	Das Sophie-Hedwig-Gymnasium veranstaltet verschiedene Projekte unter dem Namen „Sophie Goes Green“ und hat dabei bereits 10.000 € an Greenpeace spenden können.

•	Nicolaus-August-Otto-Schule Diez (NAOS) ist offiziell fairtrade-Schule

•	Karl-von-Ibell Grundschule hat anlässlich ihres 60-jährigen Bestehens unter dem Motto „Nachhaltigkeit“ eine Projektwoche und Schulfest veranstaltet. 

•	Der 2019 neu gegründete Lions Club Diez Oranien hat als Hauptziel die Nachhaltigkeit. Er führt regelmäßig plogging-Aktionen durch und forstet u.a. ein städtisches Waldstück wieder auf. Außerdem hat er zwei Rollups für foodsharing gesponsert, die nun bei Infoständen oder Fairteilungen eingesetzt werden.



