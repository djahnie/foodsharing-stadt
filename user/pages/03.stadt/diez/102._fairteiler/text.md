---
title: Fairteiler
menu: Fairteiler
published: true
image_align: left
---

## Fairteiler

Es gibt bisher einen Fairteiler im Willkommenskreis Diez, einer Flüchtlingsinitiative. Diese Kooperation ist sehr bereichernd für beide Seiten. Vor Corona wurden dort 2 x wöchentlich Lebensmittel verteilt. 

