---
title: "Gemeinschaftlicher Fairteiler"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Es gibt mindestens einen öffentlichen Fairteiler mit Beteiligung der öffentlichen Hand.

Fairteiler gibt es bei euch bestimmt schon. Aber auch schon einen, der mit Beteiligung der öffentlichen Hand betrieben wird? Das kann z.B. bedeuten, dass er an einem Ort steht, der durch die öffentliche Hand verwaltet wird. Denkbar sind das Rathaus, die Stadtbibliothek oder eine Schule. Außerdem wäre es super, wenn sich nicht nur Ehrenamtliche, um das Betreiben des Fairteilers kümmern würden. Es könnte z.B. die Reinigung teilweise von Angestellten der öffentlichen Hand übernommen werden.

![](/user/images/icons/07_idea.jpg)

