---
title: "Bildungs Austausch"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Lebensmittelverschwendung wird im Rahmen von Bildungsarbeit thematisiert. Über die Umsetzung stehen die Öffentliche Hand, Bildungseinrichtung und foodsharing Gruppe im Austausch.

- Mindestens ein Bildungsangebot an einer Schule in der Unterrichtszeit.
- Mindestens ein Bildungsangebot in der außerschulischen Bildung, welche 
   kostenfrei für alle Bewohner*innen der Stadt zugänglich sein muss (z. B. 
   Lernzentren, Ferienprogramme etc.).

Ein Problem zu erkennen, seine Ursachen und Konsequenzen zu verstehen, erhöht die Motivation, dieses Problem selbst anzugehen. Deshalb ist es unglaublich wichtig, Wissen über Lebensmittelverschwendung zu verbreiten. So werden Menschen für die Problematik sensibilisiert und ihnen Handlungsmöglichkeiten mit auf den Weg gegeben.

 Ein Bildungsangebot kann z. B. ein kleiner Workshop sein, bei dem die Teilnehmenden mehr über Hintergründe des Themas Lebensmittelverschwendung lernen und anschließend gemeinsam aus gerettetem Essen gekocht wird. Weitere Ideen für Workshops und Informationen rund um das Thema findet ihr im Materiallager der [foodsharing Akademie](https://www.foodsharing-akademie.org/).


In der überregionalen Bildungs-AG auf der foodsharing Seite könnt ihr euch über eure Bildungsarbeit vor Ort austauschen, Erfahrungen teilen und neue Ideen sammeln.

![](/user/images/icons/08_idea.jpg)