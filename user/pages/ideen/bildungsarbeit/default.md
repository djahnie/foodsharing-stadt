---
title: "Bildungsarbeit"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Lebensmittelwertschätzung ist regelmäßig fester Bestandteil von Bildungsangeboten, sowohl im schulischen, als auch im außerschulischen Bereich. 

- Bestandteil des Lehrplans an Schulen/in Kitas. 
- Angebote in der außerschulischen Bildung 

Bildungspolitik ist Ländersache. Deshalb sollte das langfristige Ziel sein, Lebensmittelwertschätzung in das Curriculum des Landes aufnehmen zu lassen, bzw. im Rahmen bestehender Curricula (z. B. zur Ernährung, Mülltrennung, zu sozialen und ethischen Fragen oder geografischen Themen) zu integrieren. Hier hat jede Bildungseinrichtung und jede Lehrkraft auch einen eigenen Spielraum. Kurzfristig könnt ihr also eure einmaligen Bildungsangebote ausbauen. Zum Beispiel indem ihr regelmäßige Termine mit dem Kindergarten vereinbart oder Lehrer\*innen schult, eigene Unterrichtseinheiten zum Thema zu geben oder einen Rahmen für gemeinsame Unterrichtsplanungen schafft. 

Greift dabei gerne wieder auf das Materiallager der [foodsharing Akademie](https://www.foodsharing-akademie.org/) zurück. Indem ihr Lebensmittelwertschätzung in eurer Stadt zu einem festen Bestandteil der Bildung macht, sensibilisiert ihr viele Menschen, die das Gelernte umsetzen und weitertragen. Außerdem sammelt ihr Erfahrungen und Positivbeispiele für die Anwendung des Themas im Unterricht. Dies hilft euch auch später dabei, Vertreter\*innen der Kultusministerien zu überzeugen, Lebensmittelwertschätzung in der Bildungspolitik zu verankern. 

![](/user/images/icons/13_idea.jpg)