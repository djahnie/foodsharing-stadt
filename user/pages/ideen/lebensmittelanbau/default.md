---
title: "Lebensmittelanbau"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Es gibt Projekte, die Lebensmittelanbau im öffentlichen Raum sichtbar machen und/oder einen wertschätzenden Umgang mit Lebensmitteln und ihrer Produktion fördern (z. B. Urban Gardening oder eine lokale “Solidarische Landwirtschaft”-Gruppe).

Lebensmittel in Privathaushalten und Betrieben vor der Tonne zu retten, ist ein wichtiger Schritt hin zu mehr Lebensmittelwertschätzung. Ein solches Verhältnis zu Lebensmitteln geht allerdings viel weiter: Es schließt grundsätzlichen Respekt vor der Umwelt und ihren Ressourcen ein. Unsere Idee ist, dass Menschen, denen der Anbau von Lebensmitteln und die damit verbundenen Anstrengungen und Vorteile zugänglicher werden, Lebensmitteln einen höheren Wert beimessen. Je sichtbarer also die Produktion von Lebensmitteln im öffentlichen Raum wird, desto mehr dringt ihr Wert in das Bewusstsein der Menschen und desto mehr Menschen handeln danach. 

![](/user/images/icons/16_idea.jpg)