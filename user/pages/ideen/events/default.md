---
title: "Events"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Die lokale foodsharing Gruppe organisiert ein öffentliches Event.

Öffentlich muss nicht groß und aufwendig sein. Schon ein gemeinsames Kochen mit vor der Tonne geretteten Lebensmitteln, das öffentlich angekündigt und für jeden Menschen zugänglich ist, erfüllt diesen Punkt. Oder wie wäre es, wenn ihr etwas über die Aufbewahrung von Lebensmitteln lernt oder einen Film zum Thema zeigt? 

Je nach euren Möglichkeiten, könnte ihr natürlich auch ein großes Event planen, z. B. ein Filmfestival oder ein Sommerfest. Bezieht bei der Planung eures Events gerne eure Partner\*innen ein: Mensen, Kindergärten und kooperierende Betriebe können andere über das bevorstehende Ereignis informieren, die lokale Umweltgruppe Tipps für einen guten Film beisteuern und die Mitarbeiter\*innen der Abfallwirtschaft aus der Praxis berichten. Und vielleicht kann die Verwaltung eurer Stadt sogar einen Ort für das Event zur Verfügung stellen? 

![](/user/images/icons/06_idea.jpg)