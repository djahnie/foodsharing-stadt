---
title: "Öffentlichkeitsarbeit - Team"
description: "Gemeinsam Lebensmittel retten und teilen"
published: true
---

### Innerhalb der lokalen foodsharing Gruppe gibt es eine Öffentlichkeitsarbeits-Gruppe, die für die Koordination von Aktionen und Öffentlichkeitswirksamkeit verantwortlich ist.

Auf der foodsharing Plattform habt ihr die Möglichkeit, eine solche Öffentlichkeitsarbeits-Gruppe innerhalb eures Bezirks zu gründen. So habt ihr ein eigenes Forum, in dem ihr euch austauschen könnt. Außerdem können neue Interessierte leicht auf euch aufmerksam werden und eurer Gruppe beitreten. 

![](/user/images/icons/03_idea.jpg)

