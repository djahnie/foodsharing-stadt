---
title: 'Wir sind stolz...'
date: '12:00 03/12/2020'
taxonomy:
    category:
        - blog
first_image: 2020-03-12_Hamburg_und_Remscheid/Lebensmittelpyramide_Rathausmarkt_Mai_2016.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle: '
... <a href="https://foodsharing-staedte.org/de/stadt/hamburg">Hamburg</a> und <a href="https://foodsharing-staedte.org/de/stadt/remscheid">Remscheid</a> als neue Städte auf unserer Website willkommen heißen zu dürfen!


'
---


Es freut uns, dass sich so schnell weitere engagierte foodsharing-Gruppen
unserer Bewegung angeschlossen haben.



Die
**Hamburger foodsharing-Gruppe** ist bereits seit 2013 (!) aktiv und
setzt sich aktuell gemeinsam mit 237 kooperierenden Betrieben gegen
die Lebensmittelverschwendung ein. Darunter befinden sich Cafés und
Bäckeren, jedoch auch Kindergärten, Restaurants und Kantinen. Sogar
Drogeriemärkte konnten bereits davon überzeugt werden, dass
foodsharing eine gute Sache ist. Aktuell ist Hamburg auf dem Weg zur
foodsharing-Stadt und betreibt aktiv 14 Fairteiler, öffentliche
Lebensmittelrettungen auf Messen und Märkten sowie Info-Stände zum
Thema foodsharing. 

![Wochenmarkt_Rettung_September_2019](Wochenmarkt_Rettung_September_2019.jpg?cropResize=500,500)



Die im Jahr
2016 gegründete **foodsharing-Gruppe Remscheids** umfasst etwa 100
aktive Foodsaver (Abholer) - aber die haben es in sich: 48 Betriebe
konnten bereits überzeugt werden, mit foodsharing zu kooperieren und
werden durch die Ortgruppe betreut. Das Thema foodsharing begeistert
ganz unterschiedliche Zielgruppen: Vom Studierenden bis zur
Hausfrau/dem Hausmann ist jeder Willkommen! Dank dieser Vielfalt kann
auch  bei spontanen Hilfegesuchen von Betrieben schnell reagiert
werden: In der Regel kann das foodsharing-Team aus Remscheid
innerhalb einer Stunde vor Ort sein und so auch bei akuten Problemen
(z. B. Ausfall von Kühlgeräten) auch größere Mengen Lebensmittel
vor der Tonne retten. 

![](Remscheid-Idee-4.jpg?cropResize=500,500)

Besonders
beeindruckt uns die ausgeprägte Öffentlichkeitsarbeit in Remscheid,
die nicht nur regelmäßig zu öffentlichkeitswirksamen Aktionen
aufruft, sondern schon jetzt unsere Vision der foodsharing-Stadt lebt
und die Vernetzung mit der öffentlichen Hand sowie einen runden
Tisch  mit diversen Initiativen aus sozialen, kirchlichen oder
öffentlichen Bereich sowie Umweltorganisationen und Politik
realisiert. Kein Wunder, dass Remscheids Stadtverwaltung bereits die
Motivationserklärung unterzeichnet hat, sodass Remscheid direkt als
offizielle foosharing-Stadt in unserer Bewegung begrüßen dürfen.
Wir gratulieren euch und hoffen, schon bald mehr von euch und
weiteren Aktionen zu hören.