---
title: 'Warum foodsharing Communities revolutionär sind!'
date: '13:00 12/12/2020'
taxonomy:
    category:
        - blog
first_image:
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Wir leben in merkwürdigen Zeiten. Unter Nutzung rießiger Ackerflächen, mit
großem Energieinsatz und viel Wassernutzung werden aufwendig
Lebensmittel produziert und dann ein Drittel davon weggeworfen. Auch
in Deutschland. [1]
Gleichzeitig haben nicht alle Menschen Zugang zu Lebensmitteln, obwohl es genügend
gibt. Im aktuellen System wird es ihnen aufgrund von Tauschzwang
vorenthalten: Wer kein Geld hat, bekommt kein Essen. [2] [3]

Der Ansatz Lebensmittel einfach zu teilen anstatt sie wegzuwerfen ist
daher revolutionär. Dabei verstehen wir Revolution als
zukunftsweisenden Prozess: Altes verwerfen und Neues beginnen.
Die Tätigkeit von foodsharing bricht mit den unlogischen Dogmen des
bestehenden Systems und etabliert eine neue Herrangehensweise an den
Umgang mit Lebensmitteln und die Verteilung dieser.

Alles beginnt mit einer Gruppe von Menschen, die bewusster mit
Lebensmitteln umgehen. Eine foodsharing Gruppe entsteht. Fairteiler
werden aufgebaut, Kooperationen mit Betrieben entstehen, mehr und
mehr Menschen bekommen mit, dass mensch Lebensmittel teilen statt
wegwerfen kann. Durch Vernetzung mit anderen Gruppen und Akteuren im
Ernährungsbereich entwickelt sich die Idee immer weiter. Durch die
Zusammenarbeit mit der öffentlichen Hand vervielfältigen sich die
Möglichkeiten der Engagierten. Gemeinsam denken sie kommunale
Strukturen neu und zeigen wie es anders gehen kann. So kann zum
Beispiel ein Ernährungsrat entstehen, wodurch die
Lebensmittelversorgung in der Stadt ganz neu gedacht wird. Weg von
langen Lieferketten und rießigen Einkaufhallen, zurück zu
regionalen Kreisläufen und kleinen Geschäften. Eine Stadt der
kurzen Wege, der Lebensmittelwertschätzung und des Teilens.

Dieser Prozess von Veränderung (Transformation) entspricht der sogenannten
Keimformtheorie. Hier wird im Bestehenden das Neue bereits gelebt und
vorweggenommen. Wenn die Zeit dann gekommen ist erfolgt der Wandel
auch im Großen. Foodsharing-Städte sind Keimformen eines neuen
Ernährungsystems und damit revolutionär.

In vielleicht schon wenigen Jahren werden sich die Menschen fragen: Wie konnten wir
jemals so mit Lebensmitteln und miteinander umgehen. Wenn
Lebensmittel da sind sollen sie verteilt und nicht weggeworfen
werden. Wenn Menschen Hunger haben, sollen sie Essen bekommen.
In einer schwierigen Zeit wie heute braucht es zukunftsweisende Konzepte
die bereits gelebt werden. Sie ebenen den Weg in eine bessere
Gesellschaft.

Quellen:
[1] WWF Deutschland, 2015: Das große Wegschmeißen 
https://www.wwf.de/fileadmin/fm-wwf/Publikationen-PDF/WWF_Studie_Das_grosse_Wegschmeissen.pdf

[2] Maria Heubuch, 2019: Es ist genug für alle da
https://www.maria-heubuch.eu/entwicklung/gute-landwirtschaft-fuer-gutes-leben/es-ist-genug-fuer-alle-da

[3] Welthungerhilfe, 2020: Hunger: Verbreitung, Ursachen & Folgen
https://www.welthungerhilfe.de/hunger/?gclid=EAIaIQobChMI-byI9fal5QIVjed3Ch23egSQEAAYAyAAEgICk_D_BwE

*Von Sebastian*
