---
title: 'foodsharing in Zeiten von Corona'
date: '12:00 06/03/2020'
taxonomy:
    category:
        - blog
first_image: 2020-06-03_corona/tomato.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Innerhalb kürzester
Zeit hat COVID-19 die Welt in Beschlag genommen und das öffentliche
Leben nahezu lahm gelegt. 

In so turbulenten
Zeiten wie diesen ist der Zusammenhalt, die gegenseitige
Unterstützung und Kommunikation wichtiger denn je.
Lebensmittelwertschätzung und eine Kultur des Tauschens und Teilens,
wie z. B. die Versorgung von Bedürftigen mit Lebensmitteln sind
Teile der Vision der foodsharing-Städte-Bewegung. Die gesamte
foodsharing-Community wird in dieser Zeit vor neue Herausforderungen
gestellt und auch unsere (angehenden) foodsharing-Städte hatten alle
Hände voll zu tun, um mit der neuen Situation umzugehen. 

Neben der
Konkretisierung von hygienischen Vorgaben für Abholungen, wie z. B.
das Tragen einer Mund-Nasen-Bedeckung oder die Maßgabe, gerettete
Lebensmittel nicht in öffentlichen Verkehrsmitteln zu
transportieren, wurde sich vielerorts auch über Fairteiler der Kopf
zerbrochen. Diese wurden anschließend entweder mit entsprechender
Beschilderung und zusätzlichen Regeln ausgestattet oder sogar
vorsichtshalber komplett geschlossen. Als Lösung für die Abholung
von großen Mengen wurden komplexe Abholsysteme entwickelt, um das
Retten von Lebensmitteln ohne Gefährdung der Gesundheit der
Foodsaver zu ermöglichen. Die Lebensmittelretter*innen von
„Foodsharing Darmstadt“ haben sogar ein
neues Fairteilsystem ausgeklügelt. So bewahren sie Lebensmittel vor
dem Müll und versorgen damit Menschen, die von der Corona-Krise
besonders betroffen sind.

![Taschen](bags.jpg)

Aus Hamburg
erreichte uns dazu eine tolle E-Mail, die wir gern mit euch teilen
möchten:

“In Zeiten von
Corona steht für uns ganz klar das Motte "Menschenleben retten
geht vor Lebensmittel retten" im Vordergrund. In diesem Zuge
stehen wir nicht nur mit anderen foodsharing Städten, sondern auch
der deutschlandweiten foodsharing Hygiene AG und dem Hamburger
Ordnungsamt im engen Austausch. In Absprache haben wir zahlreiche
Regelungen für die Rettungen zu Coronazeiten eingeführt und via
Forumsthreads, Pinnwandeinträgen in Betrieben und Events
publiziert.”

So seien Abholungen
z. B. maximal zu Zweit durchzuführen, Handschuhe zu tragen und
gerettete Lebensmitteln nicht in öffentlichen Verkehrsmitteln zu
transportieren. Fairteiler wurden mit zusätzlichen Beschriftungen
versehen, um auf Abstandsregeln hinzuweisen und sicherzustellen, dass
nicht abwaschbare Lebensmittel, wie z. B. Brot, nur noch abgepackt
weitergegeben werden. Da wegen Corona viele Tafel-Ausgabestellen
vorübergehend geschlossen haben, ist  Foodsharing Hamburg als
Lebensmittelretter eingesprungen und hat deren Abholungen
aushilfsweise übernommen. Auch Lebensmittel, die aufgrund
kurzfristiger Veranstaltungsabsagen übrig waren, wurden mit großem
Einsatz vom Hamburger foodsharing-Team vor der Tonne gerettet. 

Wir finden das
Engagement unserer (angehenden) foodsharing-Städte großartig und
bedanken uns herzlich dafür. Um dieses besondere Engagement zu
belohnen, haben wir eine neue Frucht eingeführt, die den
teilnehmenden Städten verliehen werden kann. Diese zeichnet das
verantwortungsvolle Handeln in Krisensituationen aus und belohnt
Maßnahmen, die zum Schutz und/oder zur Versorgung von Bedürftigen
beitragen. Mehr dazu findet ihr bei unserer neuen Frucht [foodsharing in Zeiten von Corona](https://www.foodsharing-staedte.org/de/ideen/corona).

*Von Vroni*