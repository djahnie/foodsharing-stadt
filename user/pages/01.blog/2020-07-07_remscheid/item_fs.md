---
title: 'Remscheid: Ehrenamtspreis
2020 geht an eine Lebensmittelretterin'
date: '15:00 07/07/2020'
taxonomy:
    category:
        - blog
first_image: 2020-07-07_remscheid/zucchinis.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
subtitle:
---
Uns haben mal wieder gute Neuigkeiten aus unserer foodsharing Stadt
Remscheid erreicht: Elisabeth Erbe, eine der foodsharing
Botschafter/*innen der Remscheider Ortsgrupppe, hat den Ehrenamtspreis
2020 erhalten. Bei einer gemeinsamen Aktion von Stadtsparkasse,
Remscheider General-Anzeiger und Radio RSG erhielt sie die meisten
Stimmen.

Das Orgateam der
foodsharing Städte Bewegung gratuliert herzlich!

In Remscheid wurden
seit Oktober 2016 rund 400 000 Kilo Lebensmittel vor dem Abfall
gerettet. Rund 100 aktive Foodsaver\*innen engagieren sich in
Remscheid für mehr Lebensmittelwertschätzung und holen regelmäßig
sowie auf Abruf bei Kooperationen übrig Lebensmittel ab. Diese
werden über Fairteilergruppen und an soziale Einrichtungen
weiterverteilt.

Außerdem waren die
Lebensmittel von Remscheid Teil der Sendung “Willicks
Hauhaltshelden”, welche am 24.06.2020 auf dem WDR ausgestrahlt
wurde.

*Von Vroni*
