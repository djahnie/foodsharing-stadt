---
title: Start
sitemap:
    changefreq: monthly
hero_classes: text-dark title-h1  parallax overlay-light

content: '
<h4> Willkommen! </h4>

<span> An vielen Orten setzen sich Menschen für mehr Wertschätzung von Lebensmitteln und gegen deren Verschwendung ein. 
Wir möchten bestehende Initiativen auf dieser Seite präsentieren und Anregungen für neue Initiativen schaffen. 
<br/><br/>
Seht euch auf dieser Seite um und vielleicht seid ihr auch schon bald Teil der Bewegung!  </span> 
<br/><br/>
'

---

# foodsharing-Städte
## Gemeinsam Lebensmittelwertschätzung leben


