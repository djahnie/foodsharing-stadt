---
title: 'foodsharing.de'
hide_page_title: true
---

### foodsharing.de

Seit rund sieben Jahren rettet die mehrfach ausgezeichnete foodsharing-Bewegung täglich tonnenweise genießbare Lebensmittel vor der Entsorgung und verteilt sie ehrenamtlich und kostenfrei im Bekanntenkreis, in Obdachlosenheimen, Kindergärten und über die Plattform [www.foodsharing.de](https://foodsharing.de/). Über 297.000 Menschen aus Deutschland, Österreich und der Schweiz nutzen regelmäßig die Internetplattform im Sinne „Teile Lebensmittel, anstatt sie wegzuwerfen!“. Darüber hinaus engagieren sich rund 66.000 Menschen ehrenamtlich als Foodsaver\*innen, indem sie überproduzierte Lebensmittel von Bäckereien, Supermärkten, Kantinen und Großhändler\*innen abholen und verteilen. Das geschieht kontinuierlich, über 2.500 Mal am Tag bei über 6.000 Kooperationspartner\*innen.
