---
title: "Impressum"
description: "Impressum"
published: true
---

##Impressum


**Kontaktdaten der foodsharing-Städte Bewegung**  
E-Mail: <kontakt@foodsharing-staedte.org>

Die foodsharing-Städte Bewegung entsteht in Zusammenarbeit mit dem foodsharing e.V.  
Weitere Informationen zu foodsharing abzurufen unter foodsharing.de

foodsharing e.V.  
Neven-DuMont-Str. 14  
50667 Köln

Vertreten durch:  
Frank Bowinkelmann  
foodsharing e.V.  
Neven-DuMont-Str. 14  
50667 Köln

**Registereintrag foodsharing e.V.**

Eintragung im Vereinsregister.  
Registergericht: Amtsgericht Köln  
Registernummer: VR 17439
