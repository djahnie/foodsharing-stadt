---
title: Sebastian
published: true
image_align: left
---


### Sebastian
**Aktiv in:** Jena  
**Foodsaver\*in seit:** Oktober 2018  
**Ich unterstütze die** Teilnehmendenbegleitung und lerne mit der Webseite umzugehen.  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** Bananen (Mein Leben basiert seit foodsharing auf Bananen :)  
**Die foodsharing Stadt bedeutet für mich:** Blick auf das große Ganze und trotzdem im Lokalen aktiv werden. Eine bessere Welt schon heute, hier und direkt möglich machen.   
