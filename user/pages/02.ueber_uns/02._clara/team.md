---
title: Clara
published: true
image_align: left
---


### Clara
**Aktiv in:** Darmstadt  
**Foodsaver\*in seit:** Herbst 2015  
**Im Team bin ich** für unsere interne Organisation zuständig. Mir ist wichtig, dass sich alle wohl fühlen, neue Menschen gut ins Team finden und wir ein schönes, wertschätzendes Miteinander leben. So sind alle motiviert und bleiben hoffentlich lange dabei!  
**Über eine Rettung dieses Lebensmittels freue ich mich am meisten:** Äpfel - die kann ich immer essen. Jeden Tag, bei jedem Wetter, in jeder Lebenslage :)  
**Die foodsharing Stadt bedeutet für mich:** Dass wir Lebensmitteln den Stellenwert geben, den sie verdienen: Sie sind unsere Lebensgrundlage - unsere Ernährung ist wichtig für unser Wohlbefinden und unseren gesellschaftlichen Zusammenhalt.  
